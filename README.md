# Fiche : TP sur l'Interpréteur de commande Unix

## Sujet abordé

TP qui met en pratique l'initiation à l'interpréteur de commande UNIX (vu précédemment en cours)

## Objectifs

Programme de NSI de 1ère :

Contenus : Systèmes d'exploitation

Capacités attendues : Utiliser les commandes de base en ligne de commande

## Pré-requis

Une initiation à l'interpréteur de commande UNIX doit avoir été vu préalablement en cours.

Une fiche de résumé des principales instructions de commande pourrait s'avérer utile pour réaliser ce TP.

## Préparation

Un ordinateur avec Ubuntu que l'on pourra avoir en système d'exploitation principal, en dual boot avce windwos ou virtualisé via virtual box

## Éléments de cours

Les commandes de base qui permettent de manipuler des fichiers et des répertoires (afficher, déplacer, créer, supprimer)

## Séance pratique

Voici l'arborescence du répertoire TP que vous trouverez dans votre répertoire courant :

https://gitlab-fil.univ-lille.fr/freddy.magniez/projet-bloc-3-fiche-de-tp/uploads/13e6d02de876cb6a202ebca1ee70c169/1560330750429.png

Vous allez devoir déplacer, supprimer, créer les répertoires afin d'obtenir cette nouvelle arborescence :

https://gitlab-fil.univ-lille.fr/freddy.magniez/projet-bloc-3-fiche-de-tp/uploads/4207c205c3e6ff89839b846af81103f2/1560330672220.png

Vous ne pouvez que déplacer et supprimer les fichiers. Vous ne pouvez pas en créer.

Vous devez supprimer tous les répertoires inutiles et conserver tel quel les dossiers 1, 2 et 3 (ils doivent conserver la même adresse). Il faudra juste créer le répertoire dossier.

Remarque pour le professeur : il y a un fichier caché et un répertoire caché :

https://gitlab-fil.univ-lille.fr/freddy.magniez/projet-bloc-3-fiche-de-tp/uploads/c13c1acc54056fe5954abe75b00bec29/1560331370729.png

## QCM E3C

Voici le chemin d'un fichier  **c:\NSI\cours\mes_exercices.txt**

1) le répertoire "cours" est le répertoire racine (root).

2) Ce chemin est un chemin relatif.

3) Ce chemin est un chemin absolu.

4) "NSI" est un fichier de c:

5) Si je fais "dir NSI" ou "ls NSI" dans un interpréteur de commande, je verrai "mes_exercices.txt" et "cours" parmi la liste des répertoires et fichiers affichés.